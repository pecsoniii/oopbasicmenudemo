//Extended by the Private Iterator Inner Classes of Menu
public interface MenuIterator {
    boolean hasNext();
    MenuItem next();
    void remove();

}

