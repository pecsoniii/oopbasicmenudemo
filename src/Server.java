import java.util.*;

public class Server {


    Menu eatAtJoesMenu = new Menu();
    Scanner scan = new Scanner(System.in);


    //The server constructor has an instantiated menu object passed to it.
    public Server(Menu eatAtJoesMenu) {

        this.eatAtJoesMenu = eatAtJoesMenu;
    }


    //This prints the wholemenu
    public void printWholeMenu() {

        //A menuIterator is instantiated by calling the method of Menu that returns an AllItemsIterator instance
        MenuIterator allItemsIterator = eatAtJoesMenu.getAllItemsIterator();

        //The method passes the AllItemsIterator, which prints each MenuItem object of a given menu
        printWholeMenu(allItemsIterator);

    }


    private void printWholeMenu(MenuIterator iterator) {
        MenuItem item;
        System.out.println("All the menu items:  ");
        while (iterator.hasNext()) {
            item = iterator.next();
            System.out.println(item.getName() + ":  " + item.getPrice() + "$");

        }

    }


    //This method is similar to printWholeMenu(), but the iterator is an itemTypeIterator
    //It only prints items that correspond to the passed integer course
    public void printMenuItemType(int course) {
        MenuIterator itemTypeIterator = eatAtJoesMenu.getItemIterator(course);
        printItemTypeMenu(itemTypeIterator);

    }


    //Here, MenuItem.category is compared to the user selected course
    //The iterator than passes over MenuItems whose course number does
    //not correspond to user selection
    private void printItemTypeMenu(MenuIterator itemIterator) {
        MenuItem item;
        System.out.println("Your Selection:  ");
        while (itemIterator.hasNext()) {

            item = itemIterator.next();

            System.out.println(item.getName() + ":  " + item.getPrice() + "$");

        }

    }

    //Instantiates a HeartHealthyMenuIterator by calling the menu objects get factory method
    public void printHeartHealthyMenu() {
        MenuIterator heartHealthyMenuIterator = eatAtJoesMenu.getHeartHealthyMenuIterator();
        printHeartHealthyMenu(heartHealthyMenuIterator);
    }

    //Iterates and prints the contents of the Heart Healthy Menu Items
    private void printHeartHealthyMenu(MenuIterator healthyItr) {
        MenuItem item;
        System.out.println("Your Selection:  ");
        while (healthyItr.hasNext()) {

            item = healthyItr.next();

            System.out.println(item.getName() + ":  " + item.getPrice() + "$");

        }
    }


    //Instantiates a wallet friendly menu
    public void printWalletFriendlyMenu(double cost) {
        MenuIterator priceChecker = eatAtJoesMenu.getWalletFriendlyMenu(cost);
        printWalletFriendlyMenu(priceChecker);
    }

    private void printWalletFriendlyMenu(MenuIterator priceChecker) {
        MenuItem item;
        System.out.println("Your Wallet Friendly Selection:  ");
        while (priceChecker.hasNext()) {

            item = priceChecker.next();

            System.out.println(item.getName() + ":  " + item.getPrice() + "$");

        }
    }


    //This uses the AllItemsIterator, and offers the user the choice to remove MenuItems by iterating through each
    //If the user chooses to remove the MenuItem they enter 'd'.  If they enter any other character, the next item
    //is displayed
    public void removeFromMenu() {
        MenuItem item;
        MenuIterator itr = eatAtJoesMenu.getAllItemsIterator();


        while (itr.hasNext()) {
            item = itr.next();
            System.out.println(item.getName() + ": " + item.getPrice() + "$");
            System.out.print("Would you like to remove this item?  Enter 'd' to remove, or enter any other key to continue");
            String userChoice = scan.next();
            if (userChoice.equals("d")) {
                itr.remove();
                System.out.println("removed");
                //   removeFromMenu(itr);
                System.out.println("Item removed");
            } else {
                System.out.println("not removed");
            }

        }
    }


}
