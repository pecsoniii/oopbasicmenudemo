public class MenuItem {
    private String name;
    private int category;
    private boolean hearthealthy;
    private String price;

    public MenuItem(String name, int course, boolean hearthealthy, String price) {
        this.name = name;
        this.category = course;
        this.hearthealthy = hearthealthy;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public boolean isHearthealthy() {
        return hearthealthy;
    }

    public void setHearthealthy(boolean hearthealthy) {
        this.hearthealthy = hearthealthy;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
