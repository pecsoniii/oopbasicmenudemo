import java.util.*;

public class Driver {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        //A Menu object is instantiated so that it may be populated
        Menu menu = new Menu();
        Server server = new Server(menu);

        //MenuItems are added to the Menu ArrayList
        menu.add("Mozzerella Sticks", menu.APPETIZERS, menu.NOT_HEART_HEALTHY, "6.0");
        menu.add("Lettuce on Plate, Served Chilled", menu.APPETIZERS, menu.HEART_HEALTHY, "14.0");
        menu.add("Filet Mignon", menu.MAIN_DISH, menu.NOT_HEART_HEALTHY, "38.0");
        menu.add("Lobster Dish", menu.MAIN_DISH, menu.HEART_HEALTHY, "40.0");
        menu.add("Ice Cream", menu.DESSERT, menu.NOT_HEART_HEALTHY, "4.0");
        menu.add("Creme Brulee", menu.DESSERT, menu.NOT_HEART_HEALTHY, "4.0");
        menu.add("Pumpkin Pie", menu.DESSERT, menu.NOT_HEART_HEALTHY, "6.0");
        menu.add("Fresh Berries", menu.DESSERT, menu.HEART_HEALTHY, "4.0");
        menu.add("Grilled Sworfish", menu.MAIN_DISH, menu.HEART_HEALTHY, "38.0");
        menu.add("Chinese Hotpot!", menu.MAIN_DISH, menu.NOT_HEART_HEALTHY, "15.0");
        menu.add("Firecracker Shrimp", menu.APPETIZERS, menu.NOT_HEART_HEALTHY, "12.0");
        menu.add("Ahi Tuna, Seared", menu.APPETIZERS, menu.HEART_HEALTHY, "15.0");


        //Basic interface to demo the program

        //Prompts user for input
        System.out.println("Welcome to Joe's!  Please select your option:  " +
                "\n1.) See all items on the menu" +
                "\n2.) See a particular item on the menu" +
                "\n3.) See the Dessert Menu:" +
                "\n4.) See the Heart Healthy Menu" +
                "\n5.) Personalize your selection based on your price limit." +
                "\n9.) Remove items from the menu" +
                "\n0.) Exit the program");

        //Userinput triggers switch case
        int userchoice = scan.nextInt();
        switch (userchoice) {

            //Prints the whole menu
            case 1:
                server.printWholeMenu();
                break;

            //Prints Menu Items of a user selected choice
            case 2: {
                System.out.println("What type of item would you like to see?" +
                        "\nEnter 1 for Appetizers" +
                        "\nEnter 2 for Main Dishes" +
                        "\nEnter 3 for Dessert");
                int newuserchoice = scan.nextInt();
                server.printMenuItemType(newuserchoice);
                break;
            }

            //Prints desserts (Reused the printMenuItemType code, but instead of asking for the user
            //to select desserts, the server simply passes the corresponding dessert menu
            case 3:
                server.printMenuItemType(3);
                break;

            //Print HeartHealthy Menu Items
            case 4:
                server.printHeartHealthyMenu();
                break;

            //Prints out a menu that filters items whose prices are larger that the userinput
            case 5:
                System.out.print("Let us know how much you would like to spend, and our restaurant can provide " +
                        "you a personalized menu." + "\nEnter an upper price limit:  ");
                double newUserChoice = scan.nextDouble();
                server.printWalletFriendlyMenu(newUserChoice);
                break;

            //Remove Items
            case 9:
                server.removeFromMenu();
                System.out.println("This is your menu now:  ");
                server.printWholeMenu();
                break;

            //Exit
            case 0:
                break;
        }
    }


}











