import java.util.*;

public class Menu {
    public static final int APPETIZERS = 1;
    public static final int MAIN_DISH = 2;
    public static final int DESSERT = 3;
    public static final boolean HEART_HEALTHY = true;
    public static final boolean NOT_HEART_HEALTHY = false;
    private ArrayList<MenuItem> menuItems;


    public Menu() {
        //Instantiate an arraylist menuItems when Menu is called
        menuItems = new ArrayList<>();
    }

    //Adds a new MenuItem to menu list
    public void add(String name, int course, Boolean heartHealth, String upperPriceLimit) {
        MenuItem thisItem = new MenuItem(name, course, heartHealth, upperPriceLimit);
        menuItems.add(thisItem);

    }

    //Factory getter methods for each iterator type
    public MenuIterator getAllItemsIterator() {
        return new AllItemsIterator(menuItems);
    }

    public MenuIterator getItemIterator(int course) {
        return new ItemIterator(menuItems, course);
    }

    public MenuIterator getHeartHealthyMenuIterator() {
        return new HeartHealthyMenuIterator(menuItems);
    }

    public MenuIterator getWalletFriendlyMenu(double cost) {
        return new WalletFriendlyMenuIterator(menuItems, cost);
    }


    //Private Iterator classes

    //The HeartHealthy Iterator return only Hearth Healthy item menus
    private class HeartHealthyMenuIterator implements MenuIterator {
        private ArrayList<MenuItem> menuItems;
        private int index = 0;

        //Constructor
        public HeartHealthyMenuIterator(ArrayList<MenuItem> menuItems) {
            this.menuItems = menuItems;
        }

        //If the Arraysize is less than or equal to the index, or the current menuItem is null:   return false
        @Override
        public boolean hasNext() {
            if (index >= menuItems.size() || menuItems.get(index) == null) {
                return false;
            } else {
                return true;
            }
        }

        //Checks to see if the value of heart healthy of the current items is false, if so remove it
        //Only return HeartHealthyItems
        @Override
        public MenuItem next() {
            MenuItem menuItem = menuItems.get(index);
            if (!menuItem.isHearthealthy()) {
                while (menuItem.isHearthealthy() != true) {
                    if (hasNext())
                        menuItems.remove(index);
                    if (hasNext() == true)
                        menuItem = menuItems.get(index);

                }
            }

            index = index + 1;

            return menuItem;
        }

        @Override
        public void remove() {

        }

    }

    //The same general concept is repeated for all the Private Inner classes
    private class ItemIterator implements MenuIterator {
        private ArrayList<MenuItem> menuItems;
        private int index = 0, course;

        //Item Iterator takes in the Array List of menuItems and the course number (MenuItem type)
        public ItemIterator(ArrayList<MenuItem> menuItems, int course) {
            this.menuItems = menuItems;
            this.course = course;
        }


        public void remove() {
        }

        @Override
        public boolean hasNext() {
            if (index >= menuItems.size() || menuItems.get(index) == null) {
                return false;
            } else {
                return true;
            }
        }

        //Here only menuItems of the corresponding course are returneed
        //I use this for both the specific menu Item requirement, and the the dessert menu requirement
        //Instead of creating a separate method, I simply provide course number 3, which is defined as desserts
        //in the Menu class.
        @Override
        public MenuItem next() {
            MenuItem menuItem = menuItems.get(index);
            if (menuItem.getCategory() != course) {
                while (menuItem.getCategory() != course) {
                    if (hasNext())
                        menuItems.remove(index);
                    if (hasNext() == true)
                        menuItem = menuItems.get(index);

                }
            }

            index = index + 1;

            return menuItem;
        }
    }


    private class AllItemsIterator implements MenuIterator {
        private ArrayList<MenuItem> menuItems;
        private int index = 0;

        public AllItemsIterator(ArrayList<MenuItem> menuItems) {
            this.menuItems = menuItems;
        }

        //Method that affect the physical structure of the array

        public void remove() {

            menuItems.remove(index - 1);
            if (index != 0)
                index--;
        }

        @Override
        public boolean hasNext() {
            if (index >= menuItems.size() || menuItems.get(index) == null) {
                return false;
            } else {
                return true;
            }
        }

        @Override
        public MenuItem next() {
            MenuItem menuItem = menuItems.get(index);
            index = index + 1;
            return menuItem;
        }
    }

    private class WalletFriendlyMenuIterator implements MenuIterator {
        private ArrayList<MenuItem> menuItems;
        private int index = 0;
        private double cost;

        public WalletFriendlyMenuIterator(ArrayList<MenuItem> menuItems, double cost) {
            this.menuItems = menuItems;
            this.cost = cost;
        }

        //Method that affect the physical structure of the array

        public void remove() {

        }

        @Override
        public boolean hasNext() {
            if (index >= menuItems.size() || menuItems.get(index) == null) {
                return false;
            } else {
                return true;
            }
        }

        @Override
        public MenuItem next() {
            MenuItem menuItem = menuItems.get(index);
            //Here only items less than the double received are returned
            if (Double.valueOf(menuItem.getPrice()) >= cost) {
                while (Double.valueOf(menuItem.getPrice()) >= cost) {
                    if (hasNext())
                        menuItems.remove(index);
                    if (hasNext() == true)
                        menuItem = menuItems.get(index);

                }
            }

            index = index + 1;

            return menuItem;
        }
    }

}
